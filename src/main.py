import uvicorn
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware


app = FastAPI()

origins = [
    "http://localhost:3000",
    "http://localhost:8000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

forms = {
    "aa3795": [
        {"name": "name", "type": "string", "mandatory": True},
        {"name": "email", "type": "email", "mandatory": True},
    ],
    "foo": [
        {"name": "question", "type": "string", "mandatory": True},
        {"name": "answer", "type": "string", "mandatory": False},
    ],
}


class FormSubmission(BaseModel):
    name: str
    email: str


@app.get("/form/{id}")
def get_form_description(id: str):
    if forms.get(id):
        return forms[id]
    else:
        raise HTTPException(status_code=404, detail="huh?!")


@app.post("/form/{id}", tags=["forms"])
def submit_form(id: str, form_data: FormSubmission):
    print("Submitted form data:")
    print(form_data.dict())
    return {"message": "Form submitted successfully"}
